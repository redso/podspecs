#
# Be sure to run `pod lib lint NAME.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = "RSUtils"
  s.version          = "0.1.3"
  s.summary          = "RSUtils"
  s.description      = "RSUtils is cool"
  s.homepage         = "http://bitbucket.org/redso/rsutils"
  #s.screenshots      = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Paulo Lam" => "paulo@beyond-six.com" }
  s.source           = { :git => "http://bitbucket.org/redso/rsutils.git", :tag => s.version.to_s }
  #s.social_media_url = 'https://twitter.com/EXAMPLE'

  s.platform     = :ios, '6.0'
  # s.ios.deployment_target = '5.0'
  # s.osx.deployment_target = '10.7'
  s.requires_arc = true

  s.source_files = 'Classes','Classes/UDID','Classes/Base64','Classes/CHSlideController','Classes/FOPLoadingIndicator','Classes/LocationManager','Classes/PullRefreshController','Classes/Regex','Classes/RSLAdvImagePicker','Classes/RSLBannerAdView','Classes/RSLDate','Classes/RSLDeepCopy','Classes/RSLImageLabelListView','Classes/RSLLabel','Classes/RSLNetwork','Classes/RSLOverlayHelper','Classes/RSLPListHelper','Classes/RSLSplashAdView','Classes/RSLTextField','Classes/RSLTextView','Classes/SBJSON','Classes/SVWebViewController'
  #s.resources = 'Assets/*.png'

  # s.ios.exclude_files = 'Classes/osx'
  # s.osx.exclude_files = 'Classes/ios'
  # s.public_header_files = 'Classes/**/*.h'
  # s.frameworks = 'SomeFramework', 'AnotherFramework'
  s.dependency 'RegexKitLite'
end
